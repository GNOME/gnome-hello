# Polish translation for gnome-hello help.
# Copyright © 2017 the gnome-hello authors.
# This file is distributed under the same license as the gnome-hello help.
# Piotr Drąg <piotrdrag@gmail.com>, 2017.
# Aviary.pl <community-poland@mozilla.org>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-hello-help\n"
"POT-Creation-Date: 2017-09-15 22:17+0000\n"
"PO-Revision-Date: 2017-09-16 00:15+0200\n"
"Last-Translator: Piotr Drąg <piotrdrag@gmail.com>\n"
"Language-Team: Polish <community-poland@mozilla.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""
"Piotr Drąg <piotrdrag@gmail.com>, 2017\n"
"Aviary.pl <community-poland@mozilla.org>, 2017"

#. (itstool) path: info/desc
#: C/index.page:6
msgid "Help for GNOME Hello."
msgstr "Pomoc programu GNOME Hello."

#. (itstool) path: title/media
#. This is a reference to an external file such as an image or video. When
#. the file changes, the md5 hash will change to let you know you need to
#. update your localized copy. The msgstr is not used at all. Set it to
#. whatever you like once you have updated your copy of the file.
#: C/index.page:7 C/index.page:26
msgctxt "_"
msgid ""
"external ref='figures/gnome-hello-logo.png' "
"md5='1ae47b7a7c4fbeb1f6bb72c0cf18d389'"
msgstr ""
"external ref='figures/gnome-hello-logo.png' "
"md5='1ae47b7a7c4fbeb1f6bb72c0cf18d389'"

#. (itstool) path: info/title
#: C/index.page:8
msgctxt "text"
msgid "GNOME Hello"
msgstr "GNOME Hello"

#. (itstool) path: credit/name
#: C/index.page:11 C/what-is.page:10 C/what-is-like.page:11
#: C/what-can-do.page:10
msgid "Milo Casagrande"
msgstr "Milo Casagrande"

#. (itstool) path: credit/name
#: C/index.page:15 C/compiling.page:10
msgid "Tiffany Antopolski"
msgstr "Tiffany Antopolski"

#. (itstool) path: license/p
#: C/index.page:20 C/compiling.page:15 C/what-is.page:15 C/what-is-like.page:16
#: C/what-can-do.page:15
msgid "Creative Commons Share Alike 3.0"
msgstr "Creative Commons Share Alike 3.0"

#. (itstool) path: page/title
#: C/index.page:26
msgid ""
"<media type=\"image\" src=\"figures/gnome-hello-logo.png\">GNOME Hello logo</"
"media>GNOME Hello"
msgstr ""
"<media type=\"image\" src=\"figures/gnome-hello-logo.png\">Logo programu "
"GNOME Hello</media> GNOME Hello"

#. (itstool) path: section/title
#: C/index.page:28
msgid "Introduction"
msgstr "Wprowadzenie"

#. (itstool) path: section/title
#: C/index.page:32
msgid "It's a small world"
msgstr "Jaki ten świat mały"

#. (itstool) path: section/title
#: C/index.page:36
msgid "Advanced"
msgstr "Zaawansowane"

#. (itstool) path: info/desc
#: C/compiling.page:7
msgid ""
"<link xref=\"compiling#compile\">Compile</link> and <link xref="
"\"compiling#run\">run</link> and <link xref=\"compiling#help\">more</link>."
msgstr ""
"<link xref=\"compiling#compile\">Kompilacja</link>, <link xref="
"\"compiling#run\">wykonywanie</link> i <link xref=\"compiling#help\">więcej</"
"link>."

#. (itstool) path: page/title
#: C/compiling.page:21
msgid "Get <app>GNOME Hello</app>"
msgstr "Skąd wziąć <app>GNOME Hello</app>"

#. (itstool) path: section/title
#: C/compiling.page:24
msgid "Compile"
msgstr "Kompilacja"

#. (itstool) path: section/p
#: C/compiling.page:25
msgid "To compile the app from source:"
msgstr "Aby skompilować program ze źródła:"

#. (itstool) path: item/p
#: C/compiling.page:29
msgid "In a terminal, type:"
msgstr "Wpisz w terminalu:"

#. (itstool) path: item/p
#: C/compiling.page:30
msgid "<cmd>git clone git://git.gnome.org/gnome-hello</cmd>"
msgstr "<cmd>git clone git://git.gnome.org/gnome-hello</cmd>"

#. (itstool) path: item/p
#: C/compiling.page:31
msgid "<cmd>cd gnome-hello</cmd>"
msgstr "<cmd>cd gnome-hello</cmd>"

#. (itstool) path: item/p
#: C/compiling.page:32
msgid "<cmd>./autogen.sh --prefix=`pwd`/install</cmd>"
msgstr "<cmd>./autogen.sh --prefix=`pwd`/install</cmd>"

#. (itstool) path: item/p
#: C/compiling.page:33
msgid "<cmd>make</cmd>"
msgstr "<cmd>make</cmd>"

#. (itstool) path: item/p
#: C/compiling.page:34
msgid "<cmd>make install</cmd>"
msgstr "<cmd>make install</cmd>"

#. (itstool) path: section/title
#: C/compiling.page:39
msgid "Run"
msgstr "Wykonywanie"

#. (itstool) path: section/p
#: C/compiling.page:40
msgid "To run gnome-hello type:"
msgstr "Aby wykonać gnome-hello, wpisz:"

#. (itstool) path: section/p
#: C/compiling.page:43
msgid "<cmd>./src/gnome-hello</cmd>"
msgstr "<cmd>./src/gnome-hello</cmd>"

#. (itstool) path: section/title
#: C/compiling.page:49
msgid "View most up-to-date help pages"
msgstr "Wyświetlanie aktualnych stron pomocy"

#. (itstool) path: section/p
#: C/compiling.page:50
msgid ""
"To run most recent help pages (and view your changes if you made any) type:"
msgstr ""
"Aby wyświetlić najnowsze strony pomocy (i zobaczyć wprowadzone zmiany), "
"wpisz:"

#. (itstool) path: section/p
#: C/compiling.page:53
msgid "<cmd>yelp help/C/</cmd>"
msgstr "<cmd>yelp help/C/</cmd>"

#. (itstool) path: info/desc
#: C/what-is.page:7
msgid "Description of <app>GNOME Hello</app>."
msgstr "Opis programu <app>GNOME Hello</app>."

#. (itstool) path: page/title
#: C/what-is.page:21
msgid "What is <app>GNOME Hello</app>"
msgstr "Czym jest <app>GNOME Hello</app>"

#. (itstool) path: page/p
#: C/what-is.page:23
msgid ""
"<app>GNOME Hello</app> is a very complex program, with many nice features "
"and nice options."
msgstr ""
"<app>GNOME Hello</app> to bardzo skomplikowany program z wieloma fajnymi "
"funkcjami i przydatnymi opcjami."

#. (itstool) path: info/desc
#: C/what-is-like.page:8
msgid "Pictures of <app>GNOME Hello</app>."
msgstr "Zdjęcia programu <app>GNOME Hello</app>."

#. (itstool) path: page/title
#: C/what-is-like.page:22
msgid "What does <app>GNOME Hello</app> look like?"
msgstr "Jak wygląda <app>GNOME Hello</app>?"

#. (itstool) path: page/p
#: C/what-is-like.page:24
msgid "This is how <app>GNOME Hello</app> looks like."
msgstr "Tak wygląda <app>GNOME Hello</app>."

#. (itstool) path: p/media
#. This is a reference to an external file such as an image or video. When
#. the file changes, the md5 hash will change to let you know you need to
#. update your localized copy. The msgstr is not used at all. Set it to
#. whatever you like once you have updated your copy of the file.
#: C/what-is-like.page:28
msgctxt "_"
msgid ""
"external ref='figures/gnome-hello-new.png' "
"md5='8a1fcc6f46a22a1f500cfef9ca51b481'"
msgstr ""
"external ref='figures/gnome-hello-new.png' "
"md5='8a1fcc6f46a22a1f500cfef9ca51b481'"

#. (itstool) path: page/p
#: C/what-is-like.page:27
msgid ""
"<media type=\"image\" mime=\"img/png\" src=\"figures/gnome-hello-new.png\"> "
"<app>GNOME Hello</app> fantastic interface. </media>"
msgstr ""
"<media type=\"image\" mime=\"img/png\" src=\"figures/gnome-hello-new.png\"> "
"Fantastyczny interfejs <app>GNOME Hello</app>. </media>"

#. (itstool) path: info/desc
#: C/what-can-do.page:7
msgid "What can you do with <app>GNOME Hello</app>."
msgstr "Co można robić za pomocą <app>GNOME Hello</app>."

#. (itstool) path: page/title
#: C/what-can-do.page:21
msgid "What <app>GNOME Hello</app> does"
msgstr "Co robi <app>GNOME Hello</app>"

#. (itstool) path: page/p
#: C/what-can-do.page:23
msgid "At the moment <app>GNOME Hello</app> can't do many things."
msgstr "W tej chwili <app>GNOME Hello</app> niewiele może."

#. (itstool) path: page/p
#: C/what-can-do.page:26
msgid ""
"This is just a demonstration on how to install help files and how to use "
"Mallard, the new syntax for writing GNOME documentation."
msgstr ""
"To tylko demonstracja, jak instalować pliki pomocy i jak używać nowej "
"składni dokumentacji środowiska GNOME o nazwie Mallard."

#. (itstool) path: page/p
#: C/what-can-do.page:30
msgid ""
"If you want to learn more about Mallard, see the <link href=\"http://"
"projectmallard.org/\">Project Mallard</link> home page."
msgstr ""
"Strona domowa <link href=\"http://projectmallard.org/\">Projektu Mallard</"
"link> zawiera więcej informacji."

#. (itstool) path: p/link
#: C/legal.xml:4
msgid "Creative Commons Attribution-Share Alike 3.0 United States License"
msgstr "Creative Commons Attribution-Share Alike 3.0 United States"

#. (itstool) path: license/p
#: C/legal.xml:3
msgid "This work is licensed under a <_:link-1/>."
msgstr "Na warunkach licencji <_:link-1/>."

#. (itstool) path: license/p
#: C/legal.xml:6
msgid ""
"As a special exception, the copyright holders give you permission to copy, "
"modify, and distribute the example code contained in this document under the "
"terms of your choosing, without restriction."
msgstr ""
"W drodze specjalnego wyjątku posiadacze praw autorskich udzielają pozwolenia "
"na kopiowanie, modyfikowanie i rozprowadzanie przykładowego kodu zawartego "
"w tym dokumencie na dowolnych warunkach, bez ograniczeń."
